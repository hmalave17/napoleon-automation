Feature: check window iframe
  
  @IframeWindowOpenTest @Iframe @RegressionTest
  Scenario: check window  open test 
    Given user enter the sahi page
    When user enter the indicated route
    Then user should see a window

   @IframeErrorPage404 @Iframe @RegressionTest
   Scenario: check page error
    Given user enter the sahi page
    When user enter in error page
    Then user should see a error page