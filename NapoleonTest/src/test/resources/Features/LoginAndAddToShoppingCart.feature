Feature: Register successful

	@ShoppingCartBooks @RegressionTest
  Scenario: Login and add to shopping cart of books
    Given user login
    When add books to shopping cart
    Then check that the total cost is correct
