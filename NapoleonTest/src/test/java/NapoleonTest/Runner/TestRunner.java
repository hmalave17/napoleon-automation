package NapoleonTest.Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "./src/test/resources/Features/", 
				 glue = { "NapoleonTest.StepsDefinitions" },
				 tags = "@RegressionTest"
                 )

public class TestRunner {

}    