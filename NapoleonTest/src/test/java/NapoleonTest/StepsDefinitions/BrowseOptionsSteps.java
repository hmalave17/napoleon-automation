package NapoleonTest.StepsDefinitions;

import org.openqa.selenium.WebDriver;

import NapoleonTest.Tasks.LoopOptions;
import NapoleonTest.Utils.Words;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class BrowseOptionsSteps {
	
	@Managed(driver = "Chrome")
	WebDriver herBrowser;
	Actor user = Actor.named("user"); 
	

    @When("^user select each of the options$")
    public void user_select_each_of_the_options() throws Throwable {
    	user.can(BrowseTheWeb.with(herBrowser));
    	user.attemptsTo(Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL_DEMO)));
    	user.attemptsTo(LoopOptions.List());
        
    }


}
