package NapoleonTest.StepsDefinitions;

import org.openqa.selenium.WebDriver;

import NapoleonTest.Tasks.EnterIframeErrorPage;
import NapoleonTest.Tasks.EnterIframeWindowOpenTest;
import NapoleonTest.Userinterfaces.IFrameTestPage;
import NapoleonTest.Utils.Words;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class IframeSteps {
	
	@Managed(driver = "Chrome")
	WebDriver herBrowser;
	Actor user = Actor.named("user"); 
	
	
	@Given("^user enter the sahi page$")
	public void userEnterTheSahiPage() {
		user.can(BrowseTheWeb.with(herBrowser));
		user.attemptsTo(Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL_DEMO)));
	   
	}

	@When("^user enter the indicated route$")
	public void userEnterTheIndicatedRoute() {
		user.attemptsTo(EnterIframeWindowOpenTest.Enter()); 
	}

	@Then("^user should see a window$")
	public void userShouldSeeAWindow() {
	   user.attemptsTo(Ensure.that(BrowseTheWeb.as(user).getDriver().getCurrentUrl()).isEqualToIgnoringCase(Words.URL_FRAMES_TEST_WITH_TITTLE));
	}
	
	
	@When("^user enter in error page$")
	public void userEnterInErrorPage() {
		user.attemptsTo(EnterIframeErrorPage.Page()); 
	}
	
	@Then("^user should see a error page$")
	public void userShouldSeeAErrorPage() {
		user.attemptsTo(Ensure.that(IFrameTestPage.ERROR_PAGE_MESSAGE).text().isEqualToIgnoringCase(Words.MESSAGE_404));
	} 
}  
  