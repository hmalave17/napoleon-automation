package NapoleonTest.StepsDefinitions;

import org.openqa.selenium.WebDriver;

import NapoleonTest.Models.DataBooks;
import NapoleonTest.Tasks.AddToShoppingCart;
import NapoleonTest.Tasks.CheckTotalCost;
import NapoleonTest.Tasks.DoLogin;
import NapoleonTest.Utils.Words;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Shared;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class LoginAndAddToShoppingCartSteps {

	@Managed(driver = "Chrome")
	WebDriver herBrowser;
	Actor user = Actor.named("user");

	@Shared
	DataBooks dataBooks;

	@Given("^user login$")
	public void user_login() throws Throwable {
		user.can(BrowseTheWeb.with(herBrowser));
		user.attemptsTo(
				Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL_TRAINING)));
		user.wasAbleTo(DoLogin.Do());
	}

	@When("^add books to shopping cart$")
	public void add_books_to_shopping_cart() throws Throwable {
		user.attemptsTo(AddToShoppingCart.Books());
	}

	@Then("^check that the total cost is correct$")
	public void check_that_the_total_cost_is_correct() throws Throwable {
		user.attemptsTo(CheckTotalCost.Items(Integer.valueOf(dataBooks.getJava()), Integer.valueOf(dataBooks.getRuby()),
				Integer.valueOf(dataBooks.getPython())));
	}

}
