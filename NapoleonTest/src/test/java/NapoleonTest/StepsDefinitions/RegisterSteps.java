package NapoleonTest.StepsDefinitions;

import org.openqa.selenium.WebDriver;
import NapoleonTest.Tasks.RegisterUser;
import NapoleonTest.Utils.Words;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RegisterSteps {
	
	@Managed(driver = "Chrome")
	WebDriver herBrowser;
	Actor user = Actor.named("user"); 	
	
	
	@Given("^user enter Sahi Training Site$")
	public void user_enter_Sahi_Training_Site() {
		user.can(BrowseTheWeb.with(herBrowser));
		user.attemptsTo(Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL_TRAINING)));
	}
 

	@When("^the user must fill in the fields$")
	public void the_user_must_fill_in_the_fields() {
		user.wasAbleTo(RegisterUser.Data());
	}

	@Then("^user should see the interfaces$")
	public void user_should_see_the_interfaces() throws InterruptedException {
	

	}

}
 