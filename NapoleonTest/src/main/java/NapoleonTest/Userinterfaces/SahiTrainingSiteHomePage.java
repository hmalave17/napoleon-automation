package NapoleonTest.Userinterfaces;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class SahiTrainingSiteHomePage extends PageObject {
	
	public static final Target USERNAME = Target.the("Username").located(By.name("user"));
	public static final Target PASSWORD = Target.the("Password").located(By.name("password"));
	public static final Target LOGIN = Target.the("Login").located(By.className("button"));	
	public static final Target REGISTER = Target.the("Register").located(By.xpath("/html/body/center/div/a"));
	
	
} 
        