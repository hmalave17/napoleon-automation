package NapoleonTest.Userinterfaces;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class SahiTestHomePage extends PageObject {
	
	public static final Target OPEN_SELF = Target.the("Open Self").located(By.xpath("/html/body/table/tbody/tr/td[4]/a[2]"));
	public static final Target IFRAMETEST = Target.the("Labbel Iframetest").located(By.xpath("/html/body/table/tbody/tr/td[1]/a[6]"));
	public static final Target LIST_LINKS = Target.the("List of links").located(By.tagName("a"));

}
   