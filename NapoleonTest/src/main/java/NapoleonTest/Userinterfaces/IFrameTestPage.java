package NapoleonTest.Userinterfaces;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class IFrameTestPage extends PageObject {
	
	
	public static final Target IFRAME_RIGHT_BOX = Target.the("Iframe Right box").located(By.xpath("//*[@id=\"another\"]/iframe")); 
	public static final Target OPTIONS_WINDOW_OPEN_TEST_RIGTH_BOX = Target.the("Options Window Open Test Rigth Box").located(By.xpath("/html/body/table/tbody/tr/td[1]/a[9]")); 
	public static final Target ENTERTEST = Target.the("Enter Test").located(By.id("checkRecord"));  
	public static final Target ERROR_PAGE_RIGTH_BOX = Target.the("Error Page Test Rigth Box").located(By.xpath("/html/body/table/tbody/tr/td[4]/a[9]")); 
	public static final Target ERROR_PAGE_OPTION_404 = Target.the("Error Page Test Rigth Box").located(By.xpath("/html/body/a[1]"));
	public static final Target ERROR_PAGE_MESSAGE = Target.the("Error Page Test Rigth Box").located(By.tagName("p"));
} 
