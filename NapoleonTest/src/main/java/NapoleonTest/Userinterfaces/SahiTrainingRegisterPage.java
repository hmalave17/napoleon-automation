package NapoleonTest.Userinterfaces;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SahiTrainingRegisterPage extends PageObject {
	
	public static final Target USERNAME = Target.the("Usarname").located(By.id("uid"));
	public static final Target PASSWORD = Target.the("Password").located(By.id("pid"));
	public static final Target REPEAD_PASSWORD = Target.the("Repead Password").located(By.id("pid2"));
	public static final Target GENDER_MALE = Target.the("Gender Male").located(By.xpath("/html/body/center/div/form/div/table/tbody/tr[4]/td[2]/input[1]"));
	public static final Target GENDER_FEMELE = Target.the("Gender Femele").located(By.xpath("/html/body/center/div/form/div/table/tbody/tr[4]/td[2]/input[2]"));
	public static final Target ADDRES = Target.the("Addres").located(By.id("taid"));
	public static final Target BILLING_ADDRES = Target.the("Billing Addres").located(By.id("btaid"));
	public static final Target STATE = Target.the("State").located(By.name("state"));
	public static final Target AGREE = Target.the("Agree").located(By.name("agree"));
	public static final Target REGISTER = Target.the("Register").located(By.xpath("/html/body/center/div/form/input[2]")); 
	

}
