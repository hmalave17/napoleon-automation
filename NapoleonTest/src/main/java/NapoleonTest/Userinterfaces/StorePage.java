package NapoleonTest.Userinterfaces;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class StorePage extends PageObject{
	
	public static final Target ADD_CORE_JAVA = Target.the("Add Core Java").located(By.xpath("//*[@id=\"listing\"]/tbody/tr[2]/td[4]/input"));
	public static final Target ADD_RUBY_FOR_RAILS = Target.the("Add Ruby For Rails").located(By.xpath("//*[@id=\"listing\"]/tbody/tr[3]/td[4]/input"));
	public static final Target ADD_PYTHON_COOKBOOK = Target.the("Add Phyton Cookbook").located(By.xpath("//*[@id=\"listing\"]/tbody/tr[4]/td[4]/input"));
	public static final Target BUTTON_ADD = Target.the("Button Add").located(By.xpath("//*[@id=\"available\"]/input[1]"));
	public static final Target GRAND_TOTAL = Target.the("Grand Total").located(By.id("total")); 
	
}
