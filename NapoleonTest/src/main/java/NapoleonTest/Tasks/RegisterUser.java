package NapoleonTest.Tasks;



import NapoleonTest.Interactions.Click;
import NapoleonTest.Interactions.Write;
import NapoleonTest.Models.DataRegister;
import NapoleonTest.Userinterfaces.SahiTrainingRegisterPage;
import NapoleonTest.Userinterfaces.SahiTrainingSiteHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Shared;

public class RegisterUser implements Task {
	
	@Shared
	DataRegister dataRegister;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.On(SahiTrainingSiteHomePage.REGISTER)); 
		actor.attemptsTo(Write.On(SahiTrainingRegisterPage.USERNAME, dataRegister.getUsername()));
		actor.attemptsTo(Write.On(SahiTrainingRegisterPage.PASSWORD, dataRegister.getPassword()));
		actor.attemptsTo(Write.On(SahiTrainingRegisterPage.REPEAD_PASSWORD, dataRegister.getPassword()));
		actor.attemptsTo(Click.On(SahiTrainingRegisterPage.GENDER_MALE));
		actor.attemptsTo(Write.On(SahiTrainingRegisterPage.ADDRES, dataRegister.getAddress()));
		actor.attemptsTo(Write.On(SahiTrainingRegisterPage.BILLING_ADDRES, dataRegister.getBuilding_address()));
		actor.attemptsTo(SelectFromOptions.byVisibleText(dataRegister.getState()).from(SahiTrainingRegisterPage.STATE));
		actor.attemptsTo(Click.On(SahiTrainingRegisterPage.AGREE));
		actor.attemptsTo(Click.On(SahiTrainingRegisterPage.REGISTER));

	}

	public static RegisterUser Data() {
		return Tasks.instrumented(RegisterUser.class); 
	}
}
