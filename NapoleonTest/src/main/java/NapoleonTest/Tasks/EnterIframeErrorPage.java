package NapoleonTest.Tasks;

import NapoleonTest.Interactions.Click;
import NapoleonTest.Userinterfaces.IFrameTestPage;
import NapoleonTest.Userinterfaces.SahiTestHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Switch;

public class EnterIframeErrorPage implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.On(SahiTestHomePage.IFRAMETEST));
		actor.attemptsTo(Switch.toFrame(IFrameTestPage.IFRAME_RIGHT_BOX.resolveFor(actor)));
		actor.attemptsTo(Click.On(IFrameTestPage.ERROR_PAGE_RIGTH_BOX)); 
		actor.attemptsTo(Click.On(IFrameTestPage.ERROR_PAGE_OPTION_404));
		
	}

	public static EnterIframeErrorPage Page() {
		return Tasks.instrumented(EnterIframeErrorPage.class); 
	}
}
