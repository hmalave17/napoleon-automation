package NapoleonTest.Tasks;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.ensure.Ensure;

public class CheckTotalCost implements Task {

	
	private int java;
	private int ruby;
	private int python;
	private final int costJava = 300;
	private final int costRuby = 200;
	private final int costPhyton = 350;
	
	
	public CheckTotalCost (int java, int ruby, int python) {
	
		this.java = java; 
		this.ruby = ruby;
		this.python = python; 

	}
	
	  
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		int total = ((java*costJava)+(ruby*costRuby)+(python*costPhyton)); 
		JavascriptExecutor js = (JavascriptExecutor) BrowseTheWeb.as(actor).getDriver();  
		WebElement asyncElement = BrowseTheWeb.as(actor).getDriver().findElement(By.id("total"));
		String valueAsyncElement = (String) js.executeScript("return arguments[0].value", asyncElement); 
		actor.attemptsTo(Ensure.that(valueAsyncElement).isEqualToIgnoringCase(String.valueOf(total)));
		 
	}

	public static CheckTotalCost Items(int java, int ruby, int python) {
		return Tasks.instrumented(CheckTotalCost.class, java, ruby, python); 
	}
}
 