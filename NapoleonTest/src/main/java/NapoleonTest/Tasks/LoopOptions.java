package NapoleonTest.Tasks;

import NapoleonTest.Interactions.Loop;
import NapoleonTest.Userinterfaces.SahiTestHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;


public class LoopOptions implements Task {

	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Loop.List(SahiTestHomePage.LIST_LINKS)); 
		
	}

	public static LoopOptions List() {
		return Tasks.instrumented(LoopOptions.class); 
	}
}
