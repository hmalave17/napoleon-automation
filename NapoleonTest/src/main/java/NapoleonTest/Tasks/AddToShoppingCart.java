package NapoleonTest.Tasks;

import NapoleonTest.Interactions.Click;
import NapoleonTest.Interactions.Write;
import NapoleonTest.Models.DataBooks;
import NapoleonTest.Userinterfaces.StorePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Shared;

public class AddToShoppingCart implements Task {
	
	@Shared
	DataBooks dataBooks;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Write.On(StorePage.ADD_CORE_JAVA, dataBooks.getJava()));
		actor.attemptsTo(Write.On(StorePage.ADD_RUBY_FOR_RAILS, dataBooks.getRuby()));
		actor.attemptsTo(Write.On(StorePage.ADD_PYTHON_COOKBOOK, dataBooks.getPython()));
		actor.attemptsTo(Click.On(StorePage.BUTTON_ADD));
		 
	}
	
	
	public static AddToShoppingCart Books() {
		return Tasks.instrumented(AddToShoppingCart.class); 
	}

}
