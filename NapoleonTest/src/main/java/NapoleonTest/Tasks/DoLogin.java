package NapoleonTest.Tasks;

import NapoleonTest.Interactions.Click;
import NapoleonTest.Interactions.Write;
import NapoleonTest.Userinterfaces.SahiTrainingSiteHomePage;
import NapoleonTest.Utils.Words;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class DoLogin implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Write.On(SahiTrainingSiteHomePage.USERNAME, Words.USER_TRAINING));
		actor.attemptsTo(Write.On(SahiTrainingSiteHomePage.PASSWORD, Words.PASSWORD_TRAINING));
		actor.attemptsTo(Click.On(SahiTrainingSiteHomePage.LOGIN));
		
	}

	public static DoLogin Do() {
		return Tasks.instrumented(DoLogin.class); 
	}
}
