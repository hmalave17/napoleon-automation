package NapoleonTest.Models;

public class DataBooks {
	
	private String java;
	private String ruby;
	private String python;
	
	public DataBooks() {
		
		this.java = "2";
		this.ruby = "5";
		this.python = "2";
	}

	public String getJava() {
		return java;
	}

	public void setJava(String java) {
		this.java = java;
	}

	public String getRuby() {
		return ruby;
	}

	public void setRuby(String ruby) {
		this.ruby = ruby;
	}

	public String getPython() {
		return python;
	}

	public void setPython(String python) {
		this.python = python;
	}
	


}
