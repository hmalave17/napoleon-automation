package NapoleonTest.Models;

public class DataRegister {
	
	private String username;
	private String password;
	private String address;
	private String building_address;
	private String state;
	
	public DataRegister() {
		this.username = "Hernan";
		this.password = "1234";
		this.address = "Medellin";
		this.state = "Kerala";
		this.building_address = "San javier";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBuilding_address() {
		return building_address;
	}

	public void setBuilding_address(String building_address) {
		this.building_address = building_address;
	}

	

}
