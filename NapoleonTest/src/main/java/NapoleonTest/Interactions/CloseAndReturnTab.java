package NapoleonTest.Interactions;

import java.util.ArrayList;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class CloseAndReturnTab implements Interaction {

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		ArrayList<String> tabs = new ArrayList<String>(BrowseTheWeb.as(actor).getDriver().getWindowHandles());
		
		BrowseTheWeb.as(actor).getDriver().close();
		BrowseTheWeb.as(actor).getDriver().switchTo().window(tabs.get(0));
		
	}

	public static CloseAndReturnTab Windows() {
		return Tasks.instrumented(CloseAndReturnTab.class); 
	}
}
