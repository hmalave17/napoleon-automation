package NapoleonTest.Interactions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class PressEnterWithRobot implements Interaction {

	
	@Override
	public <T extends Actor> void performAs(T actor) {
		BrowseTheWeb.as(actor).waitFor(3).seconds();
		Robot r;	
		try {
			r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
	}

	public static PressEnterWithRobot On() {
		return Tasks.instrumented(PressEnterWithRobot.class); 
	}
}
