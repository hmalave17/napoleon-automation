package NapoleonTest.Interactions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;

public class OpenInANewTab implements Interaction {

	private WebElement element;
	
	public OpenInANewTab(WebElement element) {
		this.element = element; 
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		String keyString = Keys.CONTROL + Keys.SHIFT.toString() + Keys.ENTER.toString();
		element.sendKeys(keyString);
		
	}

	public static OpenInANewTab Window(WebElement element) {
		return Tasks.instrumented(OpenInANewTab.class, element); 
	}
}
