package NapoleonTest.Interactions;

import java.util.List;
import org.openqa.selenium.WebElement;
import NapoleonTest.Userinterfaces.SahiTestHomePage;
import NapoleonTest.Utils.Words;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.targets.Target;

public class Loop implements Interaction {

	private Target optionsFromMenu;

	public Loop(Target optionsFromMenu) {
		this.optionsFromMenu = optionsFromMenu;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {

		List<WebElementFacade> optionsFromMenu = this.optionsFromMenu.resolveAllFor(actor);
		for (WebElement optionMenu : optionsFromMenu) {
			if (optionMenu.getText().equalsIgnoreCase(Words.FORM_TEST)) {
				actor.attemptsTo(OpenInANewTab.Window(optionMenu));
				actor.attemptsTo(SwitchTabWindow.change());
				actor.attemptsTo(PressEnterWithRobot.On());
				actor.attemptsTo(CloseAndReturnTab.Windows());
			} else if(optionMenu.getText().equalsIgnoreCase(Words.OPEN_SELF)) {
				actor.attemptsTo(Click.On(SahiTestHomePage.OPEN_SELF));
			}else {
				actor.attemptsTo(OpenInANewTab.Window(optionMenu));
				actor.attemptsTo(SwitchTabWindow.change());
				actor.attemptsTo(CloseAndReturnTab.Windows());				
			}

		}
	}

	public static Loop List(Target optionsFromMenu) {
		return Tasks.instrumented(Loop.class, optionsFromMenu);
	}
}
