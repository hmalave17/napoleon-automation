package NapoleonTest.Utils;

public class Words {
	
	public static final String URL_DEMO = "url_demo";
	public static final String URL_TRAINING = "url_training";
	public static final String FORM_TEST = "Form Test";
	public static final String OPEN_SELF = "Open Self";
	public static final String URL_FRAMES_TEST_WITH_TITTLE = "http://sahitest.com/demo/framesTestWithTitle.htm";
	public static final String MESSAGE_404 = "The requested URL /demo/404random.htm was not found on this server."; 
	public static final String USER_TRAINING = "test";
	public static final String PASSWORD_TRAINING = "secret";

}
