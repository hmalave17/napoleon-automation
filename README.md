<H1>Proyecto: Napoleon Automatización</H1>


El proyecto de Automatización Napoleon se elabora con la finalidad de evaluar los conocimientos en el área de Automatización de Hernan José Malave Montero, el cual se encuentra interesado ser parte de la compañia Napoleon Systems.

<H2>Este proyecto cuenta con las siguientes características:</H2>


- Lenguaje de programación: Java
- Framework de Automatización: Serenity
- Patrón de diseño: Screenplay
- Framework de soporte BDD (Behavior development driver): Cucumber
- Lenguaje de BDD (Behavior development driver): Gherkin
- Constructor de proyecto: Gradle


**Nota:** El proyecto esta diseñado para ser ejecutado en maquinas Windows y navegador Chrome.


<H2>El proyecto posee la siguiente estructura:</H2>

- Carpeta Funcional: En esta carpeta se van encontrar los siguientes archivos:

- [ ] Prueba de Conocimiento Teórica ANALISTA DE CALIDAD - Hernan Malave: El cual posee las respuestas de la prueba teórica
- [ ] Test Case - Hernan Malave: El cual posee los test case solicitados en la prueba teórica, es importante enfatizar que se adicionó un diagrama para explicar los posibles flujos de pruebas, caso ideales como rutas alternas. Este diagrama facilita a los miembros del equipo de prueba la compresión de los test case, cada pestaña dentro del archivo excel esta identificada con el respectivo indicativo del test case, para facilitar la ubicación de los diferentes test case.

- Carpeta Napoleon test:

- [ ] src/main/java/NapoleonTest/Iteractions: Este paquete maneja todas las funciones de iteraciones con elementos web, por ejemplo, realizar click en un elemento web, escribir sobre un elemento web, entre otros
- [ ] src/main/java/NapoleonTest/Models: Este paquete maneja toda la data necesaria para ejecutar las pruebas automatizadas
- [ ] src/main/java/NapoleonTest/Tasks: Este paquete maneja las tareas que va realizar el usuario para cumplir con lo esperado en la prueba automatizada, asi cumpliendo con el Patrón de diseño Screenplay
- [ ] src/main/java/NapoleonTest/Userinterface: En este paquete referenciamos los elementos de las vistas con las que vamos interactuar, asi garantizando la reusabilidad de código.
- [ ] src/main/java/NapoleonTest/Utils: En este paquete tenemos los archivos para reutilización de palabras claves de la automatización con el fin de evitar dejar código estático en nuestra automatización
- [ ] src/test/java/NapoleonTest/Runner: Este paquete contiene nuestro archivo runner el cual es encargado de ejecutar nuestra prueba
- [ ] src/test/java/NapoleonTest/StepDefinitions: Este paquete contiene nuestros archivos de steps los cuales definen el paso a paso del lado de lógica de programación
- [ ] src/test/resources/Features: Este paquete contiene nuestros archivos feature los cuales definen el escenario a evaluar del lado del BDD 
- [ ] gitignore: El archivo encargado de bloquear subir archivos no necesarios en nuestro repositorio
- [ ] serenity.properties: Es el archivo que nos ayuda a setear propiedades del Framework de Serenity

<H2>Datos adicionales del proyecto:</H2>

- Se implemento la anotación "@Share" de serenity para compartir data entre diferentes contextos
- Se implemento tags para todos los diferentes escenarios automatizados
- Se implemento tag generico llamado "@RegressionTest" para poder ejecutar toda la suite de pruebas

<H2>Pasos para obtener el proyecto de automation:</H2>

- Clonar el repositorio en la ubicación deseada, usando el comando: git clone https://gitlab.com/hmalave17/napoleon-automation.git
- Ingresar a la carpeta NapoleonTest


<H2>Pasos para ejecutar el proyecto</H2>

- Se debe primero realizar los pasos de la sección "Pasos para obtener el proyecto de automation"
- Opción 1: Abrir el proyecto en el IDE de su preferencia que soporte Java, ejecutar el archivo TestRunner, este runner por defecto esta configurado con el tag @RegressionTest para ejecutar todas las pruebas
- Opcion 2: Abrir desde la consola de windows la carpeta donde esta clonado el proyecto, la consola debe tener permisos de administrador, ejecutar el comando "gradlew test aggregate"


<H2>Generación de reporte del framework</H2>

- El reporte se generará en la carpeta /target/site/serenity abriendo el archivo index.html

<H2>Descripción de los escenarios propuestos para la automatización</H2>

Ingresar al link de IFRAME Test >> selecciona del iframe derecho el link: Window open Test. Verificar
que efectivamente se está abriendo la ventana.

| ID   | Caso   | Descripción del caso |
| ---  | ------ | ---------------------|  
| 1    | @IframeWindowOpenTest | En este escenario validamos la apertura de una nueva pestaña, realizando switch y validamos a traves de la URL | 
|   2  | @IframeErrorPage404 | En este escenario se valida el mensaje de error dentro de un iframe, teniendo que dar uso de las propiedades de switch iframe, Ensure de serenity|                                                                     
| 3 |  @Register | En este escenario se hace el registro de un usuario y se valida a través del mensaje de confirmación de un alert, enviamos datos desde el paquete de modelo definido usando la anotación  @Share para compartir contextos e interactuamos con los elementos de la página para llenar los campos con propiedades de Serenity|
| 4 |  @ShoppingCartBooks | En este escenario se realiza login y se agrega al carrito de compra tres articulos, se realiza la validación del monto total, de los valores introducidos por la interfaz y el campo costo total de la interfaz, comparandolo con el resultados esperado, para este caso se debio realizar la ejecución de la propiedad javascript executor debido que el campo costo total trabajaba de manera asyncrona y no reflejaba el valor total en la propiedad value al inspeccionarlo|
|5| @BrowserOptions | En este escenario se realizo el recorrido de todos los elementos web tipo link en  la página web mediante un bucle el cual recorre uno a uno los elementos, adicionalmente se implemento el abrir en una nueva pestaña cada elemento web tipo link debido que al momento de ingresar sobre la página y regresar, se refrescaba el DOM y por ende generaba error en los elementos previamente mapeados, se destaca que no se mapeo uno a uno los elementos web tipo link de la vista y de esta manera podemos proporcionar la reusabilidad al momento que se incorpore 1 o más links debido a que la iteración recorre todos los elementos con tag name "a", adicionalmente se tuvo que realizar dos condicionales debido que existian dos elementos web tipo links con comportamientos particulares|
